export { default as EAddCity } from '../../components/eAddCity.vue'
export { default as ECardSlider } from '../../components/eCardSlider.vue'
export { default as PageNotifications } from '../../components/pageNotifications.vue'

export const LazyEAddCity = import('../../components/eAddCity.vue' /* webpackChunkName: "components/eAddCity" */).then(c => c.default || c)
export const LazyECardSlider = import('../../components/eCardSlider.vue' /* webpackChunkName: "components/eCardSlider" */).then(c => c.default || c)
export const LazyPageNotifications = import('../../components/pageNotifications.vue' /* webpackChunkName: "components/pageNotifications" */).then(c => c.default || c)
