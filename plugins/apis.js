import Vue from "vue";

export default function ($this) {
  const apiPrefix = "data";
  const apiVer = "2.5";

  Vue.prototype.requestWetherAPI = (params, id) => {
    return requestAPI(`weather/`, "GET", params);
  };

  function requestAPI(api, method, params, header) {
    let request;

    const customHeaders = {};

    header = { ...header, ...customHeaders };

    try {
      if (method === "OPTIONS") {
        request = $this.$axios.options(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          params,
          header
        );
      }
      if (method === "PATCH") {
        request = $this.$axios.$patch(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          params,
          header
        );
      }
      if (method === "POST") {
        request = $this.$axios.$post(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          params,
          header
        );
      }
      if (method === "GET") {
        request = $this.$axios.$get(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          { params, headers: header }
        );
      }
      if (method === "PUT") {
        request = $this.$axios.$put(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          params,
          header
        );
      }
      if (method === "DELETE") {
        request = $this.$axios.$delete(
          "/" + apiPrefix + "/" + apiVer + "/" + api,
          params,
          header
        );
      }

      request.then((res) => {
        process.env.DEBUG === "on" &&
          console.log("API: " + api + " response", res);
      });

      request.catch((err) => {
        process.env.DEBUG === "on" && console.log(err);
        Vue.prototype.$bus.$emit("alert", {
          type: "mdi-alert-circle ",
          message: err.response.data.message,
        });
      });

      return request;
    } catch (e) {
      process.env.DEBUG === "on" && console.log(e);
    }
  }
}
